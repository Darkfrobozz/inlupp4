package org.ioopm.calculator.parser.tests;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.ioopm.calculator.ast.storage.Constants;
import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.ast.SymbolicExpression;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.binarybranch.*;
import org.ioopm.calculator.ast.unarybranch.*;
import org.ioopm.calculator.parser.CalculatorParser;
import org.ioopm.calculator.parser.IllegalExpressionException;

public class ParserTests {
    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {
    }

    @Test
    void rePipingToParser() {
        CalculatorParser parser = new CalculatorParser();
        Environment env = new Environment();
        
        try {
            SymbolicExpression expr = new Addition(new Constant(42), new Negation(new Constant(4711)));
            SymbolicExpression parsed_expr = parser.parse(expr.toString(), env);
            assertEquals(expr, parsed_expr);

            expr = new Log(new Addition(new Constant(42), new Addition(new Addition(new Constant(1), new Constant(2)), new Constant(4711))));
            parsed_expr = parser.parse(expr.toString(), env);
            // FIXME: Comparing two expressions that are the same should be the same, only their trees look the same
            // (1+2)+3 is same as 1+(2+3)
            assertEquals(expr.toString(), parsed_expr.toString());
        } catch (IllegalExpressionException | IOException e) {
            fail(e.getMessage());
            // TODO: handle exception.
        }
    }

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }    
}