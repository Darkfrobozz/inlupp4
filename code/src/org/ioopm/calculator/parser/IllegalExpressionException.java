package org.ioopm.calculator.parser;

public class IllegalExpressionException extends Exception {
    public IllegalExpressionException() {
        super();
    }
    public IllegalExpressionException(String msg) {
        super(msg);
    }
}
