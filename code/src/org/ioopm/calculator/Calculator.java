package org.ioopm.calculator;

import java.io.IOException;

import org.ioopm.calculator.ast.*;
import org.ioopm.calculator.ast.atombranch.Variable;
import org.ioopm.calculator.ast.commandbranch.Command;
import org.ioopm.calculator.ast.commandbranch.byteEnum;
import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.parser.*;

/**
 * A symbolic calculator
 */
class Calculator {
    private final Environment env;
    private final Environment backupEnv;
    private final CalculatorParser parser;
    private int parsedExpressions;
    private int evaluatedExpressions;
    private int fullyEvaluatedExpressions;

    public Calculator() {
        env = new Environment();
        backupEnv = new Environment();
        parser = new CalculatorParser();
        parsedExpressions = 0;
        evaluatedExpressions = 0;
        fullyEvaluatedExpressions = 0;
    }

    public static void main(String[] args) {
        Calculator calc = new Calculator();
        calc.eventLoop();
    }

    /**
     * Starts running the Calculator, continously prompting user for expressions to evaluate or commands to execute.
     */
    public void eventLoop() {

        System.out.println("Welcome to the Calculator!");
        System.out.println("This calculator primary function is to evaluate expressions");
        System.out.println("There are also commands you can enter: ");
        System.out.println("To exit - Quit");
        System.out.println("To display variables - Vars");
        System.out.println("To clear variables - Clear\n\n");

        mainloop: while (true) {
            
            SymbolicExpression expr = this.askQuestionExpression("? ");

            //This needs to be here because it controls flow
            if (expr.isCommand()) {
                Command cmd = (Command) expr;
                byte type = cmd.getCommandType();
                switch(type) {
                    case (byteEnum.Quit):
                        break mainloop;
                    case (byteEnum.Clear):
                        this.env.clear();
                        break;
                    case (byteEnum.Vars):
                        System.out.println(this.env);
                        break;
                    default:
                        System.out.println("Error: Command: '" + expr + "' not supported by calculator.");
                        break;
                }
                continue;
            }

            //Reusing expr
            expr = handleExpression(expr);
 
            //Result printing
            if(expr != null)
                System.out.println(expr);
            //State after care
            backupEnvironment();
        }

        System.out.println(this.stats());
        this.reset();
    }

    private SymbolicExpression handleExpression(SymbolicExpression expr) {
        this.parsedExpressions++;
        SymbolicExpression result = null;
        try {
            result = expr.eval(this.env);
            this.evaluatedExpressions++;
            if (result.isConstant()) {
                this.fullyEvaluatedExpressions++;
            }
            
            Variable temp = new Variable("ans");
            if (!result.equals(temp) && !result.containsA(temp)) {
                this.env.assignToVariable(result, temp);
            }

        } catch (IllegalExpressionException | ArithmeticException e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());
            restoreEnvironment();
        }
        return result;
    } 
    
    /**
     * Prompts user for a symbolic expression,
     * reprompts user until a string representing a symbolic expression is given.
     * @param question prompt.
     * @return symbolic expression represented by string given by user. 
     */
    private SymbolicExpression askQuestionExpression(String question) {
        SymbolicExpression expr = null;
        boolean parseFailed = true;
        while (parseFailed) {
            String input = this.promptUserInput(question);
            
            try {
                expr = this.parser.parse(input, env);
                parseFailed = false;
            } catch (SyntaxErrorException | IllegalExpressionException 
                    | IOException e) {
                System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());
            } 
        } 
        return expr;
    }

    /**
     * Prompts user for input via the CLI.
     * @param prompt prompt to show user when asking for input.
     * @return user input.
     */
    private String promptUserInput(String prompt) {
        System.out.print(prompt);
        return System.console().readLine();
    }
    

    /**
     * Reset calculator to initial state.
     */
    private void reset() {
        this.clearStats();
        this.env.clear();
    }

    /**
     * Save a local backup of the current environment.
     */
    private void backupEnvironment() {
        this.backupEnv.clear();
        this.backupEnv.putAll(this.env);            
    }

    /**
     * Restore the current environment to previous state.
     */
    private void restoreEnvironment() {
        this.env.clear();
        this.env.putAll(this.backupEnv);
    }

    /**
     * Resets the calculator's stats.
     */
    private void clearStats() {
        this.parsedExpressions = 0;
        this.evaluatedExpressions = 0;
        this.fullyEvaluatedExpressions = 0;
    }

    /**
     * Formats the calculator's stats into printable string.
     * @return a formatted string showing the calculator's stats.
     */
    private String stats() {
        String stats = "";
        stats += "Entered a total of '" + this.parsedExpressions + "' expressions.\n";
        stats += "  '" + this.evaluatedExpressions + "' expressions could be evaluated of which,\n";
        stats += "  '" + this.fullyEvaluatedExpressions + "' could be fully evaluated (down to a constant).";
        return stats;
    }
}