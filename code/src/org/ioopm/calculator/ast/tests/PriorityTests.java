package org.ioopm.calculator.ast.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.ioopm.calculator.ast.storage.Constants;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.binarybranch.*;
import org.ioopm.calculator.ast.unarybranch.*;
import org.ioopm.calculator.ast.commandbranch.*;

/**
 * Tests that priority relationships between expressions are correct.
 * Ensure this by testing priorities with <, >, == operators.
 */
public class PriorityTests {
    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {
    }

    @Test
    void randomPriorityTests() {
        // TODO: Implement tests for the thinkable cases.
        Addition add = new Addition(new Constant(1), new Constant(2));
        Subtraction sub = new Subtraction(new Constant(1), new Constant(2));
        Multiplication mul = new Multiplication(new Constant(1), new Constant(2));
        Division div = new Division(new Constant(1), new Constant(2));
        Assignment ass = new Assignment(new Constant(1), new Variable("x"));

        assertTrue(div.getPriority() > add.getPriority());
        assertTrue(div.getPriority() > sub.getPriority());
    }

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }    
}