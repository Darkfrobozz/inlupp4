package org.ioopm.calculator.ast.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.binarybranch.*;
import org.ioopm.calculator.ast.commandbranch.Clear;
import org.ioopm.calculator.ast.commandbranch.Quit;
import org.ioopm.calculator.ast.commandbranch.Vars;
import org.ioopm.calculator.ast.unarybranch.*;

public class ToStringTests {

    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {
    }
    @Test
    void StringTestConstants() {
        // Constant c1 = new Constant(5);
        // Constant c2 = new Constant(2);
        // Variable v = new Variable("x");
        // Addition a = new Addition(c1, v);
        // Multiplication m = new Multiplication(a, c2);
        // assertE quals("(5.0 + x) * 2.0", m.toString());

        assertEquals("1.0", new Constant(1).toString());
        assertEquals("x", new Variable("x").toString());
        assertEquals("sin(x)", new Sin(new Variable("x")).toString());
        assertEquals("cos(x)", new Cos(new Variable("x")).toString());
        assertEquals("exp(x)", new Exp(new Variable("x")).toString());
        assertEquals("log(x)", new Log(new Variable("x")).toString());
        assertEquals("-(x)", new Negation(new Variable("x")).toString());

        assertEquals("5.0 = x", new Assignment(new Constant(5), new Variable("x")).toString());
        assertEquals("5.0 + x", new Addition(new Constant(5), new Variable("x")).toString());
        assertEquals("5.0 - x", new Subtraction(new Constant(5), new Variable("x")).toString());
        assertEquals("5.0 * x", new Multiplication(new Constant(5), new Variable("x")).toString());
        assertEquals("5.0 / x", new Division(new Constant(5), new Variable("x")).toString()); 
    }

    @Test
    void addition() {
        Addition a_test = new Addition(new Constant(5), new Variable("x"));
        Subtraction s_test = new Subtraction(new Constant(5), new Variable("x"));
        Division d_test = new Division(new Constant(5), new Variable("x"));
        Multiplication m_test = new Multiplication(new Constant(5), new Variable("x"));

        assertEquals("5.0 + x + 5.0 * x", new Addition(a_test, m_test).toString());
        assertEquals("5.0 + x + 5.0 - x", new Addition(a_test, s_test).toString());
        assertEquals("5.0 - x + 5.0 - x", new Addition(s_test, s_test).toString());
        assertEquals("5.0 * x + 5.0 / x", new Addition(m_test, d_test).toString());
    }

    @Test
    void subtraction() {
        Addition a_test = new Addition(new Constant(5), new Variable("x"));
        Subtraction s_test = new Subtraction(new Constant(5), new Variable("x"));
        Division d_test = new Division(new Constant(5), new Variable("x"));
        Multiplication m_test = new Multiplication(new Constant(5), new Variable("x"));

        assertEquals("5.0 / x - 5.0 * x", new Subtraction(d_test, m_test).toString());
        assertEquals("5.0 + x - 5.0 * x", new Subtraction(a_test, m_test).toString());
        assertEquals("5.0 * x - (5.0 + x)", new Subtraction(m_test, a_test).toString());
        assertEquals("5.0 + x - (5.0 + x)", new Subtraction(a_test, a_test).toString());
        assertEquals("5.0 - x - (5.0 + x)", new Subtraction(s_test, a_test).toString());
    }
        
    @Test
    void division() {
        Addition a_test = new Addition(new Constant(5), new Variable("x"));
        Subtraction s_test = new Subtraction(new Constant(5), new Variable("x"));
        Division d_test = new Division(new Constant(5), new Variable("x"));
        Multiplication m_test = new Multiplication(new Constant(5), new Variable("x"));

        assertEquals("(5.0 - x) / (5.0 + x)", new Division(s_test, a_test).toString());
        assertEquals("5.0 * x / (5.0 * x)", new Division(m_test, m_test).toString());
        assertEquals("5.0 * x / (5.0 / x)", new Division(m_test, d_test).toString());
        assertEquals("(5.0 / x) / (5.0 * x)", new Division(d_test, m_test).toString());
        assertEquals("(5.0 / x) / (5.0 / x)", new Division(d_test, d_test).toString());
    }

    @Test
    void multiplication() {
        Addition a_test = new Addition(new Constant(5), new Variable("x"));
        Subtraction s_test = new Subtraction(new Constant(5), new Variable("x"));
        Division d_test = new Division(new Constant(5), new Variable("x"));
        Multiplication m_test = new Multiplication(new Constant(5), new Variable("x"));

        assertEquals("(5.0 - x) * (5.0 + x)", new Multiplication(s_test, a_test).toString());
        assertEquals("5.0 * x * 5.0 * x", new Multiplication(m_test, m_test).toString());
        assertEquals("5.0 * x * 5.0 / x", new Multiplication(m_test, d_test).toString());
        assertEquals("5.0 / x * 5.0 * x", new Multiplication(d_test, m_test).toString());
        assertEquals("5.0 / x * 5.0 / x", new Multiplication(d_test, d_test).toString());
    }

    @Test
    void quit() {
        Quit quit = Quit.instance();
        assertEquals("Quit", quit.toString());
    }

    @Test
    void vars() {
        Vars vars = Vars.instance();
        assertEquals("Vars", vars.toString());
    }

    @Test
    void clear() {
        Clear clear = Clear.instance();
        assertEquals("Clear", clear.toString());
    }

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }
}