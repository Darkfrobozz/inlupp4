package org.ioopm.calculator.ast.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.ioopm.calculator.ast.storage.Constants;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.binarybranch.*;
import org.ioopm.calculator.ast.commandbranch.*;
import org.ioopm.calculator.ast.unarybranch.*;

/**
 * assertEquals(obj1, obj2) calls each object's equals() method. 
 */
public class EqualsTests {

    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {
    }

    @Test
    void constant() {
        Constant const1a = new Constant(1);
        Constant const1b = new Constant(1);
        Constant const2 = new Constant(2);
        assertEquals(const1a, const1a);
        assertEquals(const1a, const1b);
        assertNotEquals(const1a, const2);
    }

    @Test
    void namedConstant() {
        Constant pi = Constants.namedConstants.get("pi");
        Constant e = Constants.namedConstants.get("e");
        Constant pi2 = new Constant(Math.PI);
        assertNotEquals(pi, e);
        assertNotEquals(pi, pi2);
        assertEquals(pi, pi);

        assertNotEquals(pi, null);
    }

    @Test
    void variable() {
        Variable varX1 = new Variable("x");
        Variable varX2 = new Variable("x");
        Variable varY = new Variable("y");
        assertEquals(varX1, varX1);
        assertEquals(varX1, varX2);
        assertNotEquals(varX1, varY);
    }

    @Test
    void sin() {
        Sin sinX1 = new Sin(new Variable("x"));
        Sin sinX2 = new Sin(new Variable("x"));
        Sin sinY = new Sin(new Variable("y"));
        assertEquals(sinX1, sinX1);
        assertEquals(sinX1, sinX2);
        assertNotEquals(sinX1, sinY);

        Sin sin1a = new Sin(new Constant(1));
        Sin sin1b = new Sin(new Constant(1));
        Sin sin2 = new Sin(new Constant(2));

        assertEquals(sin1a, sin1a);
        assertEquals(sin1a, sin1b);
        assertNotEquals(sinX1, sin2);
        assertNotEquals(sinX1, sin1a);
    }

    @Test
    void cos() {
        Cos cosX1 = new Cos(new Variable("x"));
        Cos cosX2 = new Cos(new Variable("x"));
        Cos cosY = new Cos(new Variable("y"));

        assertEquals(cosX1, cosX1);
        assertEquals(cosX1, cosX2);
        assertNotEquals(cosX1, cosY);

        Cos cos1a = new Cos(new Constant(1));
        Cos cos1b = new Cos(new Constant(1));
        Cos cos2 = new Cos(new Constant(2));
        assertEquals(cos1a, cos1a);
        assertEquals(cos1a, cos1b);
        assertNotEquals(cos1a, cos2);
        assertNotEquals(cosX1, cos1a);
    }

    @Test
    void exp() {
        Exp expX1 = new Exp(new Variable("x"));
        Exp expX2 = new Exp(new Variable("x"));
        Exp expY = new Exp(new Variable("y"));
        assertEquals(expX1, expX1);
        assertEquals(expX1, expX2);
        assertNotEquals(expX1, expY);

        Exp exp1a = new Exp(new Constant(1));
        Exp exp1b = new Exp(new Constant(1));
        Exp exp2 = new Exp(new Constant(2));
        assertEquals(exp1a, exp1a);
        assertEquals(exp1a, exp1b);
        assertNotEquals(exp1a, exp2);
        assertNotEquals(expX1, exp2);
    }

    @Test
    void log() {
        Log logX1 = new Log(new Variable("x"));
        Log logX2 = new Log(new Variable("x"));
        Log logY = new Log(new Variable("y"));
        assertEquals(logX1, logX1);
        assertEquals(logX1, logX2);
        assertNotEquals(logX1, logY);

        Log log1a = new Log(new Constant(1));
        Log log1b = new Log(new Constant(1));
        Log log2 = new Log(new Constant(2));
        assertEquals(log1a, log1a);
        assertEquals(log1a, log1b);
        assertNotEquals(log1a, log2);
        assertNotEquals(logX1, log2);
    }

    @Test
    void neg() {
        Negation negX1 = new Negation(new Variable("x"));
        Negation negX2 = new Negation(new Variable("x"));
        Negation negY = new Negation(new Variable("y"));
        assertEquals(negX1, negX1);
        assertEquals(negX1, negX2);
        assertNotEquals(negX1, negY);
        Negation neg1a = new Negation(new Constant(1));
        Negation neg1b = new Negation(new Constant(1));
        Negation neg2 = new Negation(new Constant(2));
        assertEquals(neg1a, neg1a);
        assertEquals(neg1a, neg1b);
        assertNotEquals(neg1a, neg2);
        assertNotEquals(negX1, neg2);
    }

    @Test
    void assignment() {
        Assignment ass5X1 = new Assignment(new Constant(5), new Variable("x"));
        Assignment ass5X2 = new Assignment(new Constant(5), new Variable("x"));
        Assignment ass4Y = new Assignment(new Constant(4), new Variable("y"));
        assertEquals(ass5X1, ass5X1);
        assertEquals(ass5X1, ass5X2);
        assertNotEquals(ass4Y, ass5X1);
    }
        
    @Test
    void addition() {
        Addition add5X = new Addition(new Constant(5), new Variable("x"));
        Addition addX5 = new Addition(new Variable("x"), new Constant(5));
        Addition addY4 = new Addition(new Variable("y"), new Constant(4));
        assertNotEquals(add5X, addX5);
        assertNotEquals(addY4, addX5);
    }

    @Test
    void multiplication() {
        Multiplication mul5X = new Multiplication(new Constant(5), new Variable("x"));
        Multiplication mulX5 = new Multiplication(new Variable("x"), new Constant(5));
        Multiplication mulY4 = new Multiplication(new Variable("y"), new Constant(4));
        assertNotEquals(mul5X, mulX5);
        assertNotEquals(mulY4, mulX5);
    }

    @Test
    void subtraction() {
        Subtraction sub5X1 = new Subtraction(new Constant(5), new Variable("x"));
        Subtraction sub5X2 = new Subtraction(new Constant(5), new Variable("x"));
        Subtraction subX5 = new Subtraction(new Variable("x"), new Constant(5));
        Subtraction subY4 = new Subtraction(new Variable("y"), new Constant(4));
        assertEquals(sub5X1, sub5X2);
        assertNotEquals(sub5X1, subX5);
        assertNotEquals(subY4, subX5);
    }
        
    @Test
    void division() {
        Division div5X1 = new Division(new Constant(5), new Variable("x"));
        Division div5X2 = new Division(new Constant(5), new Variable("x"));
        Division divX5 = new Division(new Variable("x"), new Constant(5));
        Division divY4 = new Division(new Variable("y"), new Constant(4));
        assertEquals(div5X1, div5X2);
        assertNotEquals(div5X1, divX5);
        assertNotEquals(divY4, divX5);
    }

    @Test
    void commands() {
        Clear clear = Clear.instance();
        Vars vars = Vars.instance();
        Quit quit = Quit.instance();

        assertEquals(clear, clear);
        assertEquals(vars, vars);
        assertEquals(quit, quit);
        
        assertNotEquals(clear, quit);
        assertNotEquals(clear, vars);
        assertNotEquals(quit, vars);
    }

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }    
}