package org.ioopm.calculator.ast.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.ioopm.calculator.ast.*;
import org.ioopm.calculator.ast.storage.*;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.binarybranch.*;
import org.ioopm.calculator.ast.unarybranch.*;
import org.ioopm.calculator.parser.CalculatorParser;
import org.ioopm.calculator.parser.IllegalExpressionException;

public class EvaluateTests {
    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {
    }

    @Test
    void unary() { 
        
        try {
            //Sin
            assertEquals(new Constant(0), new Sin(new Constant(0)).eval(new Environment()));
            assertEquals(new Constant(Math.sin(Math.PI)), new Sin(new Constant(Math.PI)).eval(new Environment()));
            assertEquals(new Constant(Math.sin(3 * Math.PI / 4)), new Sin(new Constant(3 * Math.PI / 4)).eval(new Environment()));
            assertEquals(new Constant(1), new Sin(new Constant(Math.PI / 2)).eval(new Environment()));

            //Cos
            assertEquals(new Constant(1), new Cos(new Constant(0)).eval(new Environment()));
            assertEquals(new Constant(-1), new Cos(new Constant(Math.PI)).eval(new Environment()));
            assertEquals(new Constant(Math.cos(Math.PI / 2)), new Cos(new Constant(Math.PI / 2)).eval(new Environment()));
        } catch (IllegalExpressionException e) {
            fail("An illegal expression exception" + e.getMessage());
        }
      
    }

    @Test
    void binary() {

        // Setup
        SymbolicExpression a1 = new Addition(new Constant(5), new Constant(37));
        SymbolicExpression a2 = new Addition(new Constant(2), new Constant(12));
        SymbolicExpression m1 = new Multiplication(new Constant(5), new Constant(3));
        SymbolicExpression m2 = new Multiplication(new Constant(2), new Constant(20));
        SymbolicExpression s1 = new Subtraction(new Constant(27), new Constant(14));
        SymbolicExpression s2 = new Subtraction(new Constant(30), new Constant(25));
        SymbolicExpression d1 = new Division(new Constant(30), new Constant(3));
        SymbolicExpression d2 = new Division(new Constant(9), new Constant(3));


        try {
        // addition
        assertEquals(new Constant(84), new Addition(a1, a1).eval(new Environment()));
        assertEquals(new Constant(30), new Addition(m1, m1).eval(new Environment()));
        assertEquals(new Constant(20), new Addition(d1, d1).eval(new Environment()));
        assertEquals(new Constant(26), new Addition(s1, s1).eval(new Environment()));
        // subtraction
        assertEquals(new Constant(28), new Subtraction(a1, a2).eval(new Environment()));
        assertEquals(new Constant(-28), new Subtraction(a2, a1).eval(new Environment()));
        assertEquals(new Constant(25), new Subtraction(m2, m1).eval(new Environment()));
        assertEquals(new Constant(-25), new Subtraction(m1, m2).eval(new Environment()));
        assertEquals(new Constant(7), new Subtraction(d1, d2).eval(new Environment()));
        assertEquals(new Constant(-7), new Subtraction(d2, d1).eval(new Environment()));
        assertEquals(new Constant(8), new Subtraction(s1, s2).eval(new Environment()));
        assertEquals(new Constant(-8), new Subtraction(s2, s1).eval(new Environment()));
        // multi
        assertEquals(new Constant(1764), new Multiplication(a1, a1).eval(new Environment()));
        assertEquals(new Constant(225), new Multiplication(m1, m1).eval(new Environment()));
        assertEquals(new Constant(100), new Multiplication(d1, d1).eval(new Environment()));
        assertEquals(new Constant(169), new Multiplication(s1, s1).eval(new Environment()));
        // divi
        assertEquals(new Constant(7), new Division(a1, new Addition(new Constant(3), new Constant(3))).eval(new Environment()));
        assertEquals(new Constant(3), new Division(m1, new Multiplication(new Constant(5), new Constant(1))).eval(new Environment()));
        assertEquals(new Constant(5), new Division(d1, new Division(new Constant(20), new Constant(10)) ).eval(new Environment()));
        assertEquals(new Constant(3), new Division(new Subtraction(new Constant(28), new Constant(7)), new Subtraction(new Constant(10), new Constant(3))).eval(new Environment()));
        } catch (IllegalExpressionException e) {
            fail(e.getMessage());
        }        
    }

    @Test
    void binaryWithVars () {
        try {
            Environment envX = new Environment();
            envX.assignToVariable(new Constant(5), new Variable("X"));

            assertEquals(new Constant(15), new Addition(new Constant(10), new Variable("X")).eval(envX));
            assertEquals(new Constant(5), new Subtraction(new Constant(10), new Variable("X")).eval(envX));
            assertEquals(new Constant(2), new Division(new Constant(10), new Variable("X")).eval(envX));
            assertEquals(new Constant(50), new Multiplication(new Constant(10), new Variable("X")).eval(envX));
            
            assertEquals(new Constant(2), new Assignment(new Constant(2), new Variable("X")).eval(envX));
            assertEquals(new Constant(5), new Division(new Constant(10), new Variable("X")).eval(envX));
        } catch (IllegalExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    void parser() {     
        Environment env = new Environment();
        CalculatorParser parser = new CalculatorParser();

        try {
            assertEquals(new Constant(Math.PI), parser.parse("pi", env).eval(env));
            assertEquals(new Constant(42), parser.parse("Answer", env).eval(env));
            assertEquals(new Constant(Math.E), parser.parse("e", env).eval(env));
            assertEquals(new Constant(6.022140857E23), parser.parse("L", env).eval(env));
        } catch (IOException | IllegalExpressionException e) {
            fail(e.getMessage());
        }
    }


    @Test
    void nonReducable() {
        try {
            assertEquals(new Constant(5), new Constant(5).eval(new Environment()));
            assertEquals(new Variable("x"), new Variable("x").eval(new Environment()));
            assertEquals(new Addition(new Variable("x"), new Constant(37)), new Addition(new Variable("x"), new Constant(37)).eval(new Environment())); 
        } catch (IllegalExpressionException e) {
            fail("Not correct");
        }
    }

    @Test
    void assignToConstantException() {
        Environment env = new Environment();
        CalculatorParser parser = new CalculatorParser(); 
        
        assertThrows(IllegalExpressionException.class, () -> {
            Assignment toTest = new Assignment(new Constant(2), parser.parse("Answer", env));
            toTest.eval(env);
        });
    }
    
    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }    
}
