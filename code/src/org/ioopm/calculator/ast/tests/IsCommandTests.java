package org.ioopm.calculator.ast.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.ioopm.calculator.ast.storage.Constants;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.binarybranch.*;
import org.ioopm.calculator.ast.unarybranch.*;
import org.ioopm.calculator.ast.commandbranch.*;

public class IsCommandTests {
    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {
    }

    @Test
    void constantIsNotCommand() {
        Constant constant = new Constant(1);
        assertFalse(constant.isCommand());
    }

    @Test
    void namedConstantIsNotCommand() {
        NamedConstant pi = Constants.namedConstants.get("pi");
        assertFalse(pi.isCommand());
    }

    @Test
    void variableIsNotCommand() {
        Variable varX = new Variable("x");
        assertFalse(varX.isCommand());
    }

    @Test
    void sinIsNotCommand() {
        Sin sinX = new Sin(new Variable("x"));
        Sin sin1 = new Sin(new Constant(1));
        assertFalse(sinX.isCommand());
        assertFalse(sin1.isCommand());
    }

    @Test
    void cosIsNotCommand() {
        Cos cosX = new Cos(new Variable("x"));
        Cos cos1 = new Cos(new Constant(1));
        assertFalse(cosX.isCommand());
        assertFalse(cos1.isCommand());
    }

    @Test
    void expIsNotCommand() {
        Exp expX = new Exp(new Variable("x"));
        Exp exp1 = new Exp(new Constant(1));
        assertFalse(expX.isCommand());
        assertFalse(exp1.isCommand());
    }

    @Test
    void logIsNotCommand() {
        Log logX = new Log(new Variable("x"));
        Log log1 = new Log(new Constant(1));
        assertFalse(logX.isCommand());
        assertFalse(log1.isCommand());
    }

    @Test
    void negIsNotCommand() {
        Negation negX = new Negation(new Variable("x"));
        Negation neg1 = new Negation(new Constant(1));
        assertFalse(negX.isCommand());
        assertFalse(neg1.isCommand());
    }

    @Test
    void assignmentIsNotCommand() {
        Assignment ass5X = new Assignment(new Constant(5), new Variable("x"));
        Assignment ass4Y = new Assignment(new Constant(4), new Variable("y"));
        assertFalse(ass5X.isCommand());
        assertFalse(ass4Y.isCommand());
    }

    @Test
    void additionIsNotCommand() {
        Addition addX5 = new Addition(new Variable("x"), new Constant(5));
        Addition addY4 = new Addition(new Variable("y"), new Constant(4));
        assertFalse(addX5.isCommand());
        assertFalse(addY4.isCommand());
    }

    @Test
    void multiplicationIsNotCommand() {
        Multiplication mulX5 = new Multiplication(new Variable("x"), new Constant(5));
        Multiplication mulY4 = new Multiplication(new Variable("y"), new Constant(4));
        assertFalse(mulX5.isCommand());
        assertFalse(mulY4.isCommand());
    }

    @Test
    void subtractionIsNotCommand() {
        Subtraction subX5 = new Subtraction(new Variable("x"), new Constant(5));
        Subtraction subY4 = new Subtraction(new Variable("y"), new Constant(4));
        assertFalse(subX5.isCommand());
        assertFalse(subY4.isCommand());
    }
     
    @Test
    void divisionIsNotCommand() {
        Division div5X = new Division(new Constant(5), new Variable("x"));
        Division divY4 = new Division(new Variable("y"), new Constant(4));
        assertFalse(div5X.isCommand());
        assertFalse(divY4.isCommand());
    }

    @Test
    void quitIsCommand() {
        Quit quit = Quit.instance();
        assertTrue(quit.isCommand());
    }

    @Test
    void varsIsCommand() {
        Vars vars = Vars.instance();
        assertTrue(vars.isCommand());
    }

    @Test
    void clearIsCommand() {
        Clear clear = Clear.instance();
        assertTrue(clear.isCommand());
    }

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }    
}