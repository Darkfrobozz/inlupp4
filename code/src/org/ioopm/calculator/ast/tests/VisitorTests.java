package org.ioopm.calculator.ast.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ioopm.calculator.ast.SymbolicExpression;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.binarybranch.*;
import org.ioopm.calculator.ast.commandbranch.Clear;
import org.ioopm.calculator.ast.commandbranch.Quit;
import org.ioopm.calculator.ast.commandbranch.Vars;
import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.ast.unarybranch.*;
import org.ioopm.calculator.ast.visitors.EvaluationVisitor;

public class VisitorTests {

    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {
    }

    @Test
    void addSub() {
        EvaluationVisitor evaluator = new EvaluationVisitor();
        Environment env = new Environment();

        Addition add = new Addition(new Constant(0), new Constant(2));
        SymbolicExpression add_eval = evaluator.evaluate(add, env);
        assertEquals(2.0, add_eval.getValue());

        Subtraction sub = new Subtraction(new Constant(0), new Constant(2));
        SymbolicExpression sub_eval = evaluator.evaluate(sub, env);
        assertEquals(-2.0, sub_eval.getValue());
        
        Addition addtree = new Addition(add, sub);
        SymbolicExpression addtree_eval = evaluator.evaluate(addtree, env);
        assertEquals(0, addtree_eval.getValue());
    }

    @Test
    void multiply() {
        EvaluationVisitor evaluator = new EvaluationVisitor();
        Environment env = new Environment();

        SymbolicExpression temp = new Multiplication(new Constant(5.0), new Constant(2.0));
        SymbolicExpression temp_eval = evaluator.evaluate(temp, env);
        assertEquals(10.0, temp_eval.getValue());

        temp = new Multiplication(temp, new Constant(2));
        temp_eval = evaluator.evaluate(temp, env);
        assertEquals(20.0, temp_eval.getValue()); 
    }

    @Test
    void binary() {

    }

    @Test
    void command() {

    }
    

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }
}
