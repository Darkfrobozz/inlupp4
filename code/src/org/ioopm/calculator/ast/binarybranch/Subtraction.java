package org.ioopm.calculator.ast.binarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
/**
 * Represents the subtraction operation in the AST.
 */
public class Subtraction extends Binary {
    public Subtraction(SymbolicExpression lhs, SymbolicExpression rhs) {
        super(lhs, rhs);
    }

    @Override
    public double operate(double lhs, double rhs) {
        return lhs - rhs;
    }

    @Override
    public String getName() {
        return "-";
    }

    public boolean isSubtraction() {
        return true;
    }
    
    @Override
    public int getPriority() {
        return 50;
    }
}
