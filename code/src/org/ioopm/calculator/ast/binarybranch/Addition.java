package org.ioopm.calculator.ast.binarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
/**
 * Represents the addition of two expressions.
 */
public class Addition extends Binary {
    public Addition(SymbolicExpression lhs, SymbolicExpression rhs) {
        super(lhs, rhs);
    }

    @Override
    public double operate(double lhs, double rhs) {
        return lhs + rhs;
    }

    @Override
    public String getName() {
        return "+";
    }

    @Override
    public int getPriority() {
        return 50;
    }
}
