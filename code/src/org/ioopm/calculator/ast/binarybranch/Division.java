package org.ioopm.calculator.ast.binarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
/**
 * Represents the division operator in the AST
 */
public class Division extends Binary {
    public Division(SymbolicExpression lhs, SymbolicExpression rhs) {
        super(lhs, rhs);
    }

    @Override
    public double operate(double lhs, double rhs) {
        return lhs / rhs;
    }

    @Override
    public String getName() {
        return "/";
    }

    @Override
    public boolean isDivision() {
        return true;
    }

    @Override
    public int getPriority() {
        return 80;
    }
}
