package org.ioopm.calculator.ast.binarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;

/**
 * Represents the multiplication operation in the AST.
 */
public class Multiplication extends Binary {
    public Multiplication(SymbolicExpression lhs, SymbolicExpression rhs) {
        super(lhs, rhs);
    }
   
    @Override
    public double operate(double lhs, double rhs) {
        return lhs * rhs;
    }

    @Override
    public String getName() {
        return "*";
    }

    @Override
    public int getPriority() {
        return 80;
    }
}
