package org.ioopm.calculator.ast.binarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.parser.IllegalExpressionException;
import org.ioopm.calculator.ast.atombranch.Constant;

/**
 * Represents an operation on two expressions in the abstract syntax tree (AST).
 */
public abstract class Binary extends SymbolicExpression {
    private SymbolicExpression lhs = null;
    private SymbolicExpression rhs = null;
    public Binary(SymbolicExpression lhs, SymbolicExpression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    /**
     * Return the expression on the left hand side of the binary operator
     * @return the binary operator's left hand side expression
     */
    public SymbolicExpression getLeftHandSide() {
        return this.lhs;
    }

    /**
     * Return the expression on the right hand side of the binary operator
     * @return the binary operator's right hand side expression
     */
    public SymbolicExpression getRightHandSide() {
        return this.rhs;
    }

    @Override
    public String toString() {
        String left = this.lhs.toString();
        String right = this.rhs.toString();
        if (this.getPriority() > this.rhs.getPriority() ||
            (this.getPriority() == this.rhs.getPriority() && (this.isSubtraction() || this.isDivision()))) {
            right = parentheses(right);
        }
        if (this.getPriority() > this.lhs.getPriority() ||
            (this.isDivision() && this.lhs.isDivision())) {
            left = parentheses(left);
        }
        return left + " " + this.getName() + " " + right;
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other) && equals((Binary) other);
    }

    public boolean equals(Binary other) {
        return other.lhs.equals(this.lhs) && other.rhs.equals(this.rhs);
    }

    @Override
    public SymbolicExpression eval(Environment vars) throws IllegalExpressionException {
        SymbolicExpression rhs_eval = rhs.eval(vars);
        SymbolicExpression lhs_eval = lhs.eval(vars);

        if (rhs_eval.isConstant() && lhs_eval.isConstant())
        {
            double result = operate(lhs_eval.getValue(), rhs_eval.getValue());

            if (Double.isInfinite(result) || Double.isNaN(result)) {
                throw new ArithmeticException("Infite/undefined result by " + this.toString());
            }

            return new Constant(result);
        }

        try {
            Binary copy = this.getClass().getConstructor(SymbolicExpression.class, SymbolicExpression.class).newInstance(lhs_eval, rhs_eval);
            return copy;
        } catch (Exception e) {
            throw new RuntimeException("Copy Failed", e);
        }
    }

    public double operate(double lhs, double rhs) {
        return 0;
    }
    

    /**
     * Adds enclosing parentesis to a string.
     * @param str string to add parenteses to.
     * @return string surrounded by parenteses.
     */
    private static String parentheses(String str) {
        return "(" + str + ")";
    }
    
    @Override
    public boolean containsA(Object other) {
        return super.containsA(other) || lhs.containsA(other) || rhs.containsA(other);
    }
}
