package org.ioopm.calculator.ast.binarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
import org.ioopm.calculator.parser.IllegalExpressionException;
import org.ioopm.calculator.ast.atombranch.Variable;
import org.ioopm.calculator.ast.storage.Environment;

/**
 * Represents the assignment of an expression to a variable.
 */
public class Assignment extends Binary {
    public Assignment(SymbolicExpression lhs, SymbolicExpression rhs) {
        super(lhs, rhs);
    }

    @Override
    public SymbolicExpression eval(Environment env) throws IllegalExpressionException {
        SymbolicExpression rhs = this.getRightHandSide();
        if (rhs.isVariable()) {
            Variable var = (Variable) rhs;
            SymbolicExpression lhs = this.getLeftHandSide().eval(env);
            if (lhs.containsA(var)) {
                throw new IllegalExpressionException("Error: Attempting recursive define");
            }
            env.assignToVariable(lhs, var);
            return lhs;
        } else {
            throw new IllegalExpressionException("Error: Expected assignment to variable," + 
                                                 "instead got assignment to expression '" + rhs + "'");
        }
    }

    @Override
    public String getName() {
        return "=";
    }
}
