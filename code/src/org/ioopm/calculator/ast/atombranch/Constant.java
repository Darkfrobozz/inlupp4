package org.ioopm.calculator.ast.atombranch;

import org.ioopm.calculator.ast.storage.Environment;

/**
 * Represents a constant in the AST.
 */
public class Constant extends Atom {
    private double value;
    public Constant(double value) {
        this.value = value;
    }

    @Override
    public Constant eval(Environment env) {
        return new Constant(this.value);
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public double getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return String.valueOf(this.value);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Constant) {
            Constant c = (Constant) other;
            return this.value == c.value;
        } else {
            return false;
        }
    }
}
