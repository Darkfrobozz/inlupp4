package org.ioopm.calculator.ast.atombranch;

/**
 * Representation of a named constant in the AST.
 * E.g. 'pi', 'euler's number' etc.
 */
public class NamedConstant extends Constant {
    private String symbol = null;
    public NamedConstant(String symbol, double value) {
        super(value);
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return this.symbol;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof NamedConstant) {
            NamedConstant nC = (NamedConstant) other;
            return this.symbol.equals(nC.symbol) && super.equals(nC);
        } else {
            return false;
        }
    }
}
