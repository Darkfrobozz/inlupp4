package org.ioopm.calculator.ast.atombranch;

import org.ioopm.calculator.ast.SymbolicExpression;
import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.parser.IllegalExpressionException;

/**
 * Represents a variable in the AST.
 * @example new Variable("x"); // Returns a Variable object with identifier "x".
 */
public class Variable extends Atom implements Comparable<Variable> {
    private String identifier;
    public Variable(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public int compareTo(Variable otherVariable) {
        return this.toString().compareTo(otherVariable.toString());
    }

    @Override
    public SymbolicExpression eval(Environment env) throws IllegalExpressionException {
        if (env.containsVariable(this)) {
            return env.getExprFromVariable(this).eval(env);
        } else {
            return new Variable(this.identifier);
        }
    }

    @Override
    public String toString() {
        return this.identifier;
    }

    @Override
    public boolean isVariable() {
        return true;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Variable) {
            Variable v = (Variable) other;
            return this.identifier.equals(v.identifier);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.identifier.hashCode();
    }

    public static void main(String[] args) {
        Variable a1 = new Variable("a");
        Variable a2 = new Variable("a");
        Variable b = new Variable("b");
        System.out.println("a = " + a1.hashCode() + "\na = " + a2.hashCode());
        System.out.println("a = " + a1.hashCode() + "\nb = " + b.hashCode());
    }
}
