package org.ioopm.calculator.ast.visitors;
import java.util.Stack;

import javax.management.RuntimeErrorException;

import org.ioopm.calculator.ast.SymbolicExpression;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.binarybranch.*;
import org.ioopm.calculator.ast.commandbranch.Clear;
import org.ioopm.calculator.ast.commandbranch.Quit;
import org.ioopm.calculator.ast.commandbranch.Vars;
import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.ast.unarybranch.*;

public abstract class VisitorBase implements Visitor {
    protected Stack<SymbolicExpression> expressionStack = new Stack<>();
    protected Environment env;


    //These are helperMethods used by Visitors they do not require any info to be on the stack before use!!!
    //However, the results will be posted in preserved order to the stack given
//----------------------------------------------------------------------------//
    //When extending this remember to preserve expected order of stack
    /**
     * 
     * @param topLevel
     * @param stack - Any stack that you want to save the results in, 
     * results are contractual bound to come out left on top and rightmost at bottom
     * @param n The visitor to traverse the trees
     */
    static void addTraversedTrees(SymbolicExpression topLevel, Stack<SymbolicExpression> stack, Visitor n) {
        if(topLevel instanceof Binary) {
            stack.add(((Binary) topLevel).getRightHandSide().accept(n));
            stack.add(((Binary) topLevel).getLeftHandSide().accept(n));
        } else if (topLevel instanceof Unary) {
            stack.add(((Unary) topLevel).getArgument().accept(n));
        }
    }

    //When extending this remember to preserve expected order of stack
    /**
     * 
     * @param topLevel
     * @param stack - Any stack that you want to save the results in, 
     * results are contractual bound to come out left on top and rightmost at bottom
     */
    static void addSubTrees(SymbolicExpression topLevel, Stack<SymbolicExpression> stack) {
        if(topLevel instanceof Binary) {
            stack.add(((Binary) topLevel).getRightHandSide());
            stack.add(((Binary) topLevel).getLeftHandSide());
        } else if (topLevel instanceof Unary) {
            stack.add(((Unary) topLevel).getArgument());
        }
    }

    //Used when wanting to push a copy onto the stack
    /**
     * 
     * @param toCopy
     * @param stack - To post results from copy operation in
     */
    static void pushCopy(SymbolicExpression toCopy, Stack<SymbolicExpression> stack) {
        try {
            SymbolicExpression copy = toCopy;
            if (toCopy instanceof Binary) {
                VisitorBase.addSubTrees(toCopy, stack);
                copy = toCopy.getClass().getConstructor(SymbolicExpression.class, SymbolicExpression.class)
                                    .newInstance(stack.pop(), stack.pop());
            } else if (toCopy instanceof Unary) {
                VisitorBase.addSubTrees(toCopy, stack);
                copy = toCopy.getClass().getConstructor(SymbolicExpression.class)
                                    .newInstance(stack.pop());
            } else if (toCopy instanceof Atom) {
                if (toCopy.isVariable()) {
                    copy = toCopy.getClass().getConstructor(String.class)
                                        .newInstance(toCopy.toString());
                } else if(toCopy.isConstant()){
                    copy = toCopy.getClass().getConstructor(double.class)
                                       .newInstance(toCopy.getValue());
                }
            }
            stack.add(copy);
        } catch (Exception e) {
            throw new RuntimeException("Copy Failed", e);
        }
    }
//----------------------------------------------------------------------------//

    //BINARY
    public SymbolicExpression visit(Addition n) {
        addTraversedTrees(n, expressionStack, this);
        return new Addition(expressionStack.pop(), expressionStack.pop());
    }
    public SymbolicExpression visit(Assignment n) {
        addTraversedTrees(n, expressionStack, this);
        return new Assignment(expressionStack.pop(), expressionStack.pop());
    }
    public SymbolicExpression visit(Division n) {
        addTraversedTrees(n, expressionStack, this);
        return new Division(expressionStack.pop(), expressionStack.pop());
    }
    public SymbolicExpression visit(Multiplication n) {
        addTraversedTrees(n, expressionStack, this);
        return new Multiplication(expressionStack.pop(), expressionStack.pop());
    }
    public SymbolicExpression visit(Subtraction n) {
        addTraversedTrees(n, expressionStack, this);
        return new Subtraction(expressionStack.pop(), expressionStack.pop());
    }

    //ATOMS
    public SymbolicExpression visit(Constant n) {
        return new Constant(n.getValue());
    }
    public SymbolicExpression visit(Variable n) {
        return n;
    }

    //UNARY
    public SymbolicExpression visit(Cos n) {
        addTraversedTrees(n, expressionStack, this);
        return new Cos(expressionStack.pop()); 
    }
    public SymbolicExpression visit(Exp n) {
        addTraversedTrees(n, expressionStack, this);
        return new Exp(expressionStack.pop()); 
    }
    public SymbolicExpression visit(Log n) {
        addTraversedTrees(n, expressionStack, this);
        return new Log(expressionStack.pop()); 
    }
    public SymbolicExpression visit(Negation n) {
        addTraversedTrees(n, expressionStack, this);
        return new Negation(expressionStack.pop()); 
    }
    public SymbolicExpression visit(Sin n) {
        addTraversedTrees(n, expressionStack, this);
        return new Sin(expressionStack.pop()); 
    }

    //COMMANDS
    public SymbolicExpression visit(Vars n) {
        return n;
    }
    public SymbolicExpression visit(Quit n) {
        return n;
    }
    public SymbolicExpression visit(Clear n) {
        return n;
    }
}