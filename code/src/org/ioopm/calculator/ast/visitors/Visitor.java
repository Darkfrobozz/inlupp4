package org.ioopm.calculator.ast.visitors;

import org.ioopm.calculator.ast.*;
import org.ioopm.calculator.ast.commandbranch.*;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.unarybranch.*;
import org.ioopm.calculator.ast.binarybranch.*;

public interface Visitor {
    // TODO: Enable prototype delarations for every subclass
    public SymbolicExpression visit(Addition n);
    // public SymbolicExpression visit(Assignment n);
    // public SymbolicExpression visit(Clear n);
    public SymbolicExpression visit(Constant n);
    // public SymbolicExpression visit(Cos n);
    public SymbolicExpression visit(Division n);
    // public SymbolicExpression visit(Exp n);
    // public SymbolicExpression visit(Log n);
    public SymbolicExpression visit(Multiplication n);
    // public SymbolicExpression visit(Negation n);
    // public SymbolicExpression visit(Quit n);
    // public SymbolicExpression visit(Sin n);
    public SymbolicExpression visit(Subtraction n);
    // public SymbolicExpression visit(Variable n);
    // public SymbolicExpression visit(Vars n);
}