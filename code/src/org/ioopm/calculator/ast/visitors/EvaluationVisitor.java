package org.ioopm.calculator.ast.visitors;

import java.beans.Expression;
import java.util.Stack;

import org.ioopm.calculator.ast.*;
import org.ioopm.calculator.ast.storage.*;
import org.ioopm.calculator.ast.atombranch.*;
import org.ioopm.calculator.ast.unarybranch.*;
import org.ioopm.calculator.ast.binarybranch.*;

public class EvaluationVisitor implements Visitor {
    protected Stack<SymbolicExpression> expressionStack = new Stack<>();
    protected Environment env;
    
    public SymbolicExpression evaluate(SymbolicExpression topLevel, Environment env) {
        this.env = env;
        Visitor vis = this;
        return topLevel.accept(vis);
    }
    // This method gets called from Addition.accept(Visitor v) -- you should
    // be able to see from the eval() methods how these should behave (i.e., 
    // compare this method with your Addition::eval() and Symbolic.addition) 
    // TODO: Implement evaluation for each subclass:
    // public SymbolicExpression visit(Assignment n);
    // public SymbolicExpression visit(Clear n);
    // public SymbolicExpression visit(Cos n);
    // public SymbolicExpression visit(Division n);
    // public SymbolicExpression visit(Exp n);
    // public SymbolicExpression visit(Log n);

    // public SymbolicExpression visit(Negation n);
    // public SymbolicExpression visit(Quit n);
    // public SymbolicExpression visit(Sin n);


    //Binary Assignment
    public SymbolicExpression visit(Assignment n) {
        VisitorBase.addSubTrees(n, expressionStack);    
        SymbolicExpression left = expressionStack.pop().accept(this);
        SymbolicExpression right = expressionStack.pop();

        if (right.isVariable()) {
            env.assignToVariable(left, (Variable) right);
            return left;
        } else {
            throw new RuntimeException("Tried to assign to expression");
        }
    }

    //Binary Operations
    public SymbolicExpression visit(Addition n) {
        VisitorBase.addTraversedTrees(n, expressionStack, this);
        SymbolicExpression left = expressionStack.pop();
        SymbolicExpression right = expressionStack.pop();
        if(left.isConstant() && right.isConstant()) {
            return new Constant(left.getValue() + right.getValue());
        } else {
            VisitorBase.pushCopy(n, expressionStack);
            return expressionStack.pop();
        } 
    }

    public SymbolicExpression visit(Division n) {
        VisitorBase.addTraversedTrees(n, expressionStack, this);
        SymbolicExpression left = expressionStack.pop();
        SymbolicExpression right = expressionStack.pop();
        if(left.isConstant() && right.isConstant()) {
            return new Constant(left.getValue() / right.getValue());
        } else {
            VisitorBase.pushCopy(n, expressionStack);
            return expressionStack.pop();
        } 
    } 

    public SymbolicExpression visit(Multiplication n) {
        VisitorBase.addTraversedTrees(n, expressionStack, this);
        SymbolicExpression left = expressionStack.pop();
        SymbolicExpression right = expressionStack.pop();
        if(left.isConstant() && right.isConstant()) {
            return new Constant(left.getValue() * right.getValue());
        } else {
            VisitorBase.pushCopy(n, expressionStack);
            return expressionStack.pop();
        } 
    }

    public SymbolicExpression visit(Subtraction n) {
        VisitorBase.addTraversedTrees(n, expressionStack, this);
        SymbolicExpression left = expressionStack.pop();
        SymbolicExpression right = expressionStack.pop();
        if(left.isConstant() && right.isConstant()) {
            return new Constant(left.getValue() - right.getValue());
        } else {
            VisitorBase.pushCopy(n, expressionStack);
            return expressionStack.pop();
        } 
    }

    //Atom
    public SymbolicExpression visit(Constant n) {
        VisitorBase.pushCopy(n, expressionStack);
        return expressionStack.pop();
    }
     // Rest of all visit methods
     public static void main(String[] args) {
        EvaluationVisitor evaluator = new EvaluationVisitor();
        Environment env = new Environment();

        Addition add = new Addition(new Constant(0), new Constant(2));
        System.out.print(add + " = ");
        SymbolicExpression add_eval = evaluator.evaluate(add, env);
        System.out.println(add_eval);

        Subtraction sub = new Subtraction(new Constant(0), new Constant(2));
        System.out.print(sub + " = ");
        SymbolicExpression sub_eval = evaluator.evaluate(sub, env);
        System.out.println(sub_eval);
        
        Addition addtree = new Addition(add, sub);
        System.out.print(addtree + " = ");
        SymbolicExpression addtree_eval = evaluator.evaluate(addtree, env);
        System.out.println(addtree_eval);      
     }
}