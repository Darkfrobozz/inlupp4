package org.ioopm.calculator.ast.unarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
/**
 * Representation of natural logarithm operation in the AST.
 * @example new Log(new Constant(8)); // equals mathematical operation log_e(8).
 */
public class Log extends Unary {
    public Log(SymbolicExpression expr) {
        super(expr);
    }

    @Override
    public double operate(double arg) {
        return Math.log(arg);
    }

    @Override
    public String getName() {
        return "log";
    }

    @Override public boolean equals(Object other) {
        if (other instanceof Log) {
            return super.equals(other);
        } else {
            return false;
        }
    }
}
