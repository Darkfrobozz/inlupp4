package org.ioopm.calculator.ast.unarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
/**
 * Represents the natural exponent operation in the AST.
 * @Example new Exp(new Constant(10)); // returns AST representation of mathematical operation: e^(10)
 */
public class Exp extends Unary {
    public Exp(SymbolicExpression expr) {
        super(expr);
    }
    @Override
    public double operate(double arg) {
        return Math.exp(arg);
    }

    @Override
    public String getName() {
        return "exp";
    }

    @Override public boolean equals(Object other) {
        if (other instanceof Exp) {
            return super.equals(other);
        } else {
            return false;
        }
    }
}
