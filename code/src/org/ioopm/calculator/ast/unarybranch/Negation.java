package org.ioopm.calculator.ast.unarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
/**
 * Representation of multiplying an expression with (-1) in the AST.
 */
public class Negation extends Unary {
    public Negation(SymbolicExpression expr) {
        super(expr);
    }

   
    @Override
    public double operate(double arg) {
        return -arg;
    }

    @Override
    public String getName() {
        return "-";
    }

    @Override public boolean equals(Object other) {
        if (other instanceof Negation) {
            return super.equals(other);
        } else {
            return false;
        }
    }
}
