package org.ioopm.calculator.ast.unarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
/**
 * Represents the cosinus operator in the AST.
 */
public class Cos extends Unary {
    public Cos(SymbolicExpression expr) {
        super(expr);
    }

    @Override
    public double operate(double arg) {
        return Math.cos(arg);
    }

    @Override
    public String getName() {
        return "cos";
    }

    @Override public boolean equals(Object other) {
        if (other instanceof Cos) {
            return super.equals(other);
        } else {
            return false;
        }
    }
}
