package org.ioopm.calculator.ast.unarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
/**
 * Represents sinus operation in the AST.
 */
public class Sin extends Unary {
    public Sin(SymbolicExpression expr) {
        super(expr);
    }

    @Override
    public double operate(double arg) {
        return Math.sin(arg);
    }

    @Override
    public String getName() {
        return "sin";
    }

    @Override public boolean equals(Object other) {
        if (other instanceof Sin) {
            return super.equals(other);
        } else {
            return false;
        }
    }
}
