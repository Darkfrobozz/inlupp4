package org.ioopm.calculator.ast.unarybranch;

import org.ioopm.calculator.ast.SymbolicExpression;
import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.ast.atombranch.Constant;
import org.ioopm.calculator.parser.IllegalExpressionException;

/**
 * Represents an operation on a single expression in the abstract syntax tree (AST).
 */
public abstract class Unary extends SymbolicExpression {
    private SymbolicExpression argument = null;
    public Unary(SymbolicExpression argument) {
        this.argument = argument;
    }

    /**
     * Retrieves the argument of a unary operator
     * @return the unary operator's argument.
     */
    public SymbolicExpression getArgument() {
        return this.argument;
    }

    @Override
    public SymbolicExpression eval(Environment vars) throws IllegalExpressionException {
        SymbolicExpression arg_eval = this.argument.eval(vars);
        if (arg_eval.isConstant())
            return new Constant(operate(arg_eval.getValue()));
        
        try {
            Unary copy = this.getClass().getConstructor(SymbolicExpression.class).newInstance(arg_eval);
            return copy;
        } catch (Exception e) {
            throw new RuntimeException("Copy Failed", e);
        }
    }

    public double operate(double lhs) {
        return 0;
    }

    @Override
    public String toString() {
        return this.getName() + "(" + this.argument + ")";
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other) && equals((Unary) other);
    }

    public boolean equals(Unary other) {
        return argument.equals(other.argument);
    } 
    @Override
    public boolean containsA(Object other) {
        return super.containsA(other) || argument.containsA(other);
    }
}
