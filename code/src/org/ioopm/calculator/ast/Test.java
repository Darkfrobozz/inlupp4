package org.ioopm.calculator.ast;

import java.io.IOException;

import org.ioopm.calculator.ast.atombranch.Constant;
import org.ioopm.calculator.ast.atombranch.Variable;
import org.ioopm.calculator.ast.binarybranch.Addition;
import org.ioopm.calculator.ast.binarybranch.Assignment;
import org.ioopm.calculator.ast.binarybranch.Division;
import org.ioopm.calculator.ast.binarybranch.Multiplication;
import org.ioopm.calculator.ast.storage.Constants;
import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.ast.unarybranch.Cos;
import org.ioopm.calculator.ast.unarybranch.Exp;
import org.ioopm.calculator.ast.unarybranch.Log;
import org.ioopm.calculator.ast.unarybranch.Negation;
import org.ioopm.calculator.ast.unarybranch.Sin;
import org.ioopm.calculator.ast.binarybranch.Subtraction;
import org.ioopm.calculator.parser.CalculatorParser;
import org.ioopm.calculator.parser.IllegalExpressionException;

public abstract class Test {
    protected static int testCount = 0;
    protected static int testPassed = 0;
    
    public static String results() {
        String rslt = "";
        rslt += "Tests executed: " + testCount + "\n";
        if (testCount == testPassed) {
            rslt += "OK! All tests passed!";
        } else {
            rslt += "FAIL! One or more tests failed!\n";
            rslt += "Passed: " + testPassed + "\n";
            rslt += "Failed: " + (testCount - testPassed);
        }
        return rslt;
    }

    public static void testPrinting(String expected, SymbolicExpression e) {
        testCount++;
        if (expected.equals("" + e)) {
            testPassed++;
            System.out.println("Passed: " + e);
        } else {
            System.out.println("Error: expected '" + expected + "' but got '" + e + "'");
        }
    }

    public static void testEquals(boolean expected, SymbolicExpression a, SymbolicExpression b) {
        testCount++;
        boolean r = a.equals(b);
        String testStr = a + ".equals(" + b + ")";
        if (r == expected) {
            testPassed++;
            System.out.println("Passed: " + testStr);
        } else {
            System.out.println("Error: expected '" + expected + "' but got '" + r + "': " + testStr);
        }
    }

    public static void testEvaluating(SymbolicExpression expected, SymbolicExpression e, Environment env) {
        testCount++;
        try {
            SymbolicExpression r = e.eval(env);
            if (r.equals(expected)) {
                testPassed++;
                System.out.println("Passed: " + e);
            } else {
                System.out.println("Error: expected '" + expected + "' but got '" + e + "' = " + "'" + r + "'");
            }
        } catch (IllegalExpressionException err) {
            System.out.println("Error: " + err);
        }
    }

    public static void testEvaluatingThrowsException(SymbolicExpression expected, SymbolicExpression e, Environment env) {
        testCount++;
        try {
            e.eval(env);
        } catch (IllegalExpressionException err) {
            System.out.println("Passed: " + e + ".eval()" + " threw exception: " + err);
            testPassed++;
            return;
        }
        System.out.println("Failed: Expected exception: 'IllegalExpressionException'");
    }

    /**
     * @brief Run tests to check that each SymbolicExpression in AST correctly converted to string
     */
    private static void testSuiteToString() {
        System.out.println("Test: SymbolicExpression.toString()");
        Constant c1 = new Constant(5);
        Constant c2 = new Constant(2);
        Variable v = new Variable("x");
        Addition a = new Addition(c1, v);
        Multiplication m = new Multiplication(a, c2);
        Test.testPrinting("(5.0 + x) * 2.0", m);

        Test.testPrinting("1.0", new Constant(1));
        Test.testPrinting("x", new Variable("x"));
        Test.testPrinting("sin(x)", new Sin(new Variable("x")));
        Test.testPrinting("cos(x)", new Cos(new Variable("x")));
        Test.testPrinting("exp(x)", new Exp(new Variable("x")));
        Test.testPrinting("log(x)", new Log(new Variable("x")));
        Test.testPrinting("-(x)", new Negation(new Variable("x")));

        Test.testPrinting("5.0 = x", new Assignment(new Constant(5), new Variable("x")));
        Test.testPrinting("5.0 + x", new Addition(new Constant(5), new Variable("x")));
        Test.testPrinting("5.0 - x", new Subtraction(new Constant(5), new Variable("x")));
        Test.testPrinting("5.0 * x", new Multiplication(new Constant(5), new Variable("x")));
        Test.testPrinting("5.0 / x", new Division(new Constant(5), new Variable("x")));
        
        Addition a_test = new Addition(new Constant(5), new Variable("x"));
        Subtraction s_test = new Subtraction(new Constant(5), new Variable("x"));
        Division d_test = new Division(new Constant(5), new Variable("x"));
        Multiplication m_test = new Multiplication(new Constant(5), new Variable("x"));
        
        Test.testPrinting("5.0 + x + 5.0 * x", new Addition(a_test, m_test));
        Test.testPrinting("5.0 + x + 5.0 - x", new Addition(a_test, s_test));
        Test.testPrinting("5.0 - x + 5.0 - x", new Addition(s_test, s_test));
        Test.testPrinting("5.0 * x + 5.0 / x", new Addition(m_test, d_test));

        Test.testPrinting("5.0 / x - 5.0 * x", new Subtraction(d_test, m_test));
        Test.testPrinting("5.0 + x - 5.0 * x", new Subtraction(a_test, m_test));
        Test.testPrinting("5.0 * x - (5.0 + x)", new Subtraction(m_test, a_test));
        Test.testPrinting("5.0 + x - (5.0 + x)", new Subtraction(a_test, a_test));
        Test.testPrinting("5.0 - x - (5.0 + x)", new Subtraction(s_test, a_test));
        
        Test.testPrinting("(5.0 - x) / (5.0 + x)", new Division(s_test, a_test));
        Test.testPrinting("5.0 * x / (5.0 * x)", new Division(m_test, m_test));
        Test.testPrinting("5.0 * x / (5.0 / x)", new Division(m_test, d_test));
        Test.testPrinting("(5.0 / x) / (5.0 * x)", new Division(d_test, m_test));
        Test.testPrinting("(5.0 / x) / (5.0 / x)", new Division(d_test, d_test));

        Test.testPrinting("(5.0 - x) * (5.0 + x)", new Multiplication(s_test, a_test));
        Test.testPrinting("5.0 * x * 5.0 * x", new Multiplication(m_test, m_test));
        Test.testPrinting("5.0 * x * 5.0 / x", new Multiplication(m_test, d_test));
        Test.testPrinting("5.0 / x * 5.0 * x", new Multiplication(d_test, m_test));
        Test.testPrinting("5.0 / x * 5.0 / x", new Multiplication(d_test, d_test));

        System.out.println("###############################");
    }

    /**
    * @brief tests SymbolicExpression.equals() give correct output.
    */
    private static void testSuiteEquals() {
        System.out.println("Test: SymbolicExpression.equals()");
        Constant const1a = new Constant(1);
        Constant const1b = new Constant(1);
        Constant const2 = new Constant(2);
        Test.testEquals(true, const1a, const1a);
        Test.testEquals(true, const1a, const1b);
        Test.testEquals(false, const1a, const2);

        Variable varX1 = new Variable("x");
        Variable varX2 = new Variable("x");
        Variable varY = new Variable("y");
        Test.testEquals(true, varX1, varX1);
        Test.testEquals(true, varX1, varX2);
        Test.testEquals(false, varX1, varY);

        Sin sinX1 = new Sin(new Variable("x"));
        Sin sinX2 = new Sin(new Variable("x"));
        Sin sinY = new Sin(new Variable("y"));
        Test.testEquals(true, sinX1, sinX1);
        Test.testEquals(true, sinX1, sinX2);
        Test.testEquals(false, sinX1, sinY);
        Sin sin1a = new Sin(new Constant(1));
        Sin sin1b = new Sin(new Constant(1));
        Sin sin2 = new Sin(new Constant(2));
        Test.testEquals(true, sin1a, sin1a);
        Test.testEquals(true, sin1a, sin1b);
        Test.testEquals(false, sin1a, sin2);
        Test.testEquals(false, sinX1, sin1a);

        Cos cosX1 = new Cos(new Variable("x"));
        Cos cosX2 = new Cos(new Variable("x"));
        Cos cosY = new Cos(new Variable("y"));
        Test.testEquals(true, cosX1, cosX1);
        Test.testEquals(true, cosX1, cosX2);
        Test.testEquals(false, cosX1, cosY);
        Cos cos1a = new Cos(new Constant(1));
        Cos cos1b = new Cos(new Constant(1));
        Cos cos2 = new Cos(new Constant(2));
        Test.testEquals(true, cos1a, cos1a);
        Test.testEquals(true, cos1a, cos1b);
        Test.testEquals(false, cos1a, cos2);
        Test.testEquals(false, cosX1, cos1a);

        Exp expX1 = new Exp(new Variable("x"));
        Exp expX2 = new Exp(new Variable("x"));
        Exp expY = new Exp(new Variable("y"));
        Test.testEquals(true, expX1, expX1);
        Test.testEquals(true, expX1, expX2);
        Test.testEquals(false, expX1, expY);
        Exp exp1a = new Exp(new Constant(1));
        Exp exp1b = new Exp(new Constant(1));
        Exp exp2 = new Exp(new Constant(2));
        Test.testEquals(true, exp1a, exp1a);
        Test.testEquals(true, exp1a, exp1b);
        Test.testEquals(false, exp1a, exp2);
        Test.testEquals(false, expX1, exp2);

        Log logX1 = new Log(new Variable("x"));
        Log logX2 = new Log(new Variable("x"));
        Log logY = new Log(new Variable("y"));
        Test.testEquals(true, logX1, logX1);
        Test.testEquals(true, logX1, logX2);
        Test.testEquals(false, logX1, logY);
        Log log1a = new Log(new Constant(1));
        Log log1b = new Log(new Constant(1));
        Log log2 = new Log(new Constant(2));
        Test.testEquals(true, log1a, log1a);
        Test.testEquals(true, log1a, log1b);
        Test.testEquals(false, log1a, log2);
        Test.testEquals(false, logX1, log2);

        Negation negX1 = new Negation(new Variable("x"));
        Negation negX2 = new Negation(new Variable("x"));
        Negation negY = new Negation(new Variable("y"));
        Test.testEquals(true, negX1, negX1);
        Test.testEquals(true, negX1, negX2);
        Test.testEquals(false, negX1, negY);
        Negation neg1a = new Negation(new Constant(1));
        Negation neg1b = new Negation(new Constant(1));
        Negation neg2 = new Negation(new Constant(2));
        Test.testEquals(true, neg1a, neg1a);
        Test.testEquals(true, neg1a, neg1b);
        Test.testEquals(false, neg1a, neg2);
        Test.testEquals(false, negX1, neg2);

        Assignment ass5X1 = new Assignment(new Constant(5), new Variable("x"));
        Assignment ass5X2 = new Assignment(new Constant(5), new Variable("x"));
        Assignment ass4Y = new Assignment(new Constant(4), new Variable("y"));
        Test.testEquals(true, ass5X1, ass5X1);
        Test.testEquals(true, ass5X1, ass5X2);
        Test.testEquals(false, ass4Y, ass5X1);

        Addition add5X = new Addition(new Constant(5), new Variable("x"));
        Addition addX5 = new Addition(new Variable("x"), new Constant(5));
        Addition addY4 = new Addition(new Variable("y"), new Constant(4));
        Test.testEquals(false, add5X, addX5);
        Test.testEquals(false, addY4, addX5);

        Multiplication mul5X = new Multiplication(new Constant(5), new Variable("x"));
        Multiplication mulX5 = new Multiplication(new Variable("x"), new Constant(5));
        Multiplication mulY4 = new Multiplication(new Variable("y"), new Constant(4));
        Test.testEquals(false, mul5X, mulX5);
        Test.testEquals(false, mulY4, mulX5);

        Subtraction sub5X1 = new Subtraction(new Constant(5), new Variable("x"));
        Subtraction sub5X2 = new Subtraction(new Constant(5), new Variable("x"));
        Subtraction subX5 = new Subtraction(new Variable("x"), new Constant(5));
        Subtraction subY4 = new Subtraction(new Variable("y"), new Constant(4));
        Test.testEquals(true, sub5X1, sub5X2);
        Test.testEquals(false, sub5X1, subX5);
        Test.testEquals(false, subY4, subX5);
        
        Division div5X1 = new Division(new Constant(5), new Variable("x"));
        Division div5X2 = new Division(new Constant(5), new Variable("x"));
        Division divX5 = new Division(new Variable("x"), new Constant(5));
        Division divY4 = new Division(new Variable("y"), new Constant(4));
        Test.testEquals(true, div5X1, div5X2);
        Test.testEquals(false, div5X1, divX5);
        Test.testEquals(false, divY4, divX5);

        Constant pi = Constants.namedConstants.get("pi");
        Constant e = Constants.namedConstants.get("e");
        Constant pi2 = new Constant(Math.PI);
        Test.testEquals(false, pi, e);
        Test.testEquals(false, pi, pi2);
        Test.testEquals(true, pi, pi);

        Test.testEquals(false, pi, null);

        System.out.println("###############################");
    }

    /**
    * @brief tests SymbolicExpression.eval() give correct output.
    */
    private static void testSuiteEval() {
        System.out.println("Test: SymbolicExpression.eval()");

        Test.testEvaluating(new Constant(15), new Addition(new Constant(5), new Constant(10)), new Environment());
        Test.testEvaluating(new Constant(8), new Subtraction(new Constant(10), new Constant(2)), new Environment());
        Test.testEvaluating(new Constant(20), new Multiplication(new Constant(10), new Constant(2)), new Environment());
        Test.testEvaluating(new Constant(5), new Division(new Constant(10), new Constant(2)), new Environment());
        
        Test.testEvaluating(new Constant(0), new Sin(new Constant(0)), new Environment());
        Test.testEvaluating(new Constant(Math.sin(Math.PI)), new Sin(new Constant(Math.PI)), new Environment());
        Test.testEvaluating(new Constant(Math.sin(3 * Math.PI / 4)), new Sin(new Constant(3 * Math.PI / 4)), new Environment());
        Test.testEvaluating(new Constant(1), new Sin(new Constant(Math.PI / 2)), new Environment());
        
        Test.testEvaluating(new Constant(1), new Cos(new Constant(0)), new Environment());
        Test.testEvaluating(new Constant(-1), new Cos(new Constant(Math.PI)), new Environment());
        Test.testEvaluating(new Constant(Math.cos(Math.PI / 2)), new Cos(new Constant(Math.PI / 2)), new Environment());

        Test.testEvaluating(new Constant(5), new Constant(5), new Environment());
        Test.testEvaluating(new Variable("x"), new Variable("x"), new Environment());
        Test.testEvaluating(new Addition(new Variable("x"), new Constant(37)), new Addition(new Variable("x"), new Constant(37)), new Environment());
        Test.testEvaluating(new Constant(42), new Addition(new Constant(5), new Constant(37)), new Environment());
        
        Environment envX = new Environment();
        envX.assignToVariable(new Constant(5), new Variable("x"));

        Test.testEvaluating(new Constant(15), new Addition(new Constant(10), new Variable("x")), envX);
        Test.testEvaluating(new Constant(5), new Subtraction(new Constant(10), new Variable("x")), envX);
        Test.testEvaluating(new Constant(2), new Division(new Constant(10), new Variable("x")), envX);
        Test.testEvaluating(new Constant(50), new Multiplication(new Constant(10), new Variable("x")), envX);
        
        Test.testEvaluating(new Constant(2), new Assignment(new Constant(2), new Variable("x")), envX);
        Test.testEvaluating(new Constant(5), new Division(new Constant(10), new Variable("x")), envX);

        SymbolicExpression a1 = new Addition(new Constant(5), new Constant(37));
        SymbolicExpression a2 = new Addition(new Constant(2), new Constant(12));
        SymbolicExpression m1 = new Multiplication(new Constant(5), new Constant(3));
        SymbolicExpression m2 = new Multiplication(new Constant(2), new Constant(20));
        SymbolicExpression s1 = new Subtraction(new Constant(27), new Constant(14));
        SymbolicExpression s2 = new Subtraction(new Constant(30), new Constant(25));
        SymbolicExpression d1 = new Division(new Constant(30), new Constant(3));
        SymbolicExpression d2 = new Division(new Constant(9), new Constant(3));
        // addition
        testEvaluating(new Constant(84) ,new Addition(a1, a1), new Environment());
        testEvaluating(new Constant(30) ,new Addition(m1, m1), new Environment());
        testEvaluating(new Constant(20) ,new Addition(d1, d1), new Environment());
        testEvaluating(new Constant(26) ,new Addition(s1, s1), new Environment());
        // subtraction
        testEvaluating(new Constant(28) ,new Subtraction(a1, a2), new Environment());
        testEvaluating(new Constant(-28) ,new Subtraction(a2, a1), new Environment());
        testEvaluating(new Constant(25) ,new Subtraction(m2, m1), new Environment());
        testEvaluating(new Constant(-25) ,new Subtraction(m1, m2), new Environment());
        testEvaluating(new Constant(7) ,new Subtraction(d1, d2), new Environment());
        testEvaluating(new Constant(-7) ,new Subtraction(d2, d1), new Environment());
        testEvaluating(new Constant(8) ,new Subtraction(s1, s2), new Environment());
        testEvaluating(new Constant(-8) ,new Subtraction(s2, s1), new Environment());
        // multi
        testEvaluating(new Constant(1764) ,new Multiplication(a1, a1), new Environment());
        testEvaluating(new Constant(225) ,new Multiplication(m1, m1), new Environment());
        testEvaluating(new Constant(100) ,new Multiplication(d1, d1), new Environment());
        testEvaluating(new Constant(169) ,new Multiplication(s1, s1), new Environment());
        // divi
        testEvaluating(new Constant(7) ,new Division(a1, new Addition(new Constant(3), new Constant(3))), new Environment());
        testEvaluating(new Constant(3) ,new Division(m1, new Multiplication(new Constant(5), new Constant(1))), new Environment());
        testEvaluating(new Constant(5) ,new Division(d1,new Division( new Constant(20), new Constant(10)) ), new Environment());
        testEvaluating(new Constant(3) ,new Division(new Subtraction(new Constant(28), new Constant(7)), new Subtraction(new Constant(10), new Constant(3))), new Environment());

        Environment env = new Environment();
        CalculatorParser parser = new CalculatorParser();
        try {
            Test.testEvaluating(new Constant(Math.PI), parser.parse("pi", env), env);
            Test.testEvaluating(new Constant(42), parser.parse("Answer", env), env);
            Test.testEvaluating(new Constant(Math.E), parser.parse("e", env), env);
            Test.testEvaluating(new Constant(6.022140857E23), parser.parse("L", env), env);
        } catch (IOException e) {
            System.out.println("FAILED: " + e.getMessage());
        } catch (IllegalExpressionException e) {
            System.out.println("FAILED: " + e.getMessage());
        }

        env = new Environment();
        try {
            Test.testEvaluatingThrowsException(new Constant(42), new Assignment(new Constant(2), parser.parse("Answer", env)), env);
        } catch (IOException e) {
            System.out.println("FAILED: " + e.getMessage());
        } catch (IllegalExpressionException e) {
            System.out.println("FAILED: " + e.getMessage());
        }

        env = new Environment();
        Test.testEvaluating(new Subtraction(new Constant(2), new Negation(new Variable("x"))),
                            new Assignment(new Subtraction(new Constant(2), new Negation(new Variable("x"))), new Variable("y")), env);
        Test.testEvaluating(new Subtraction(new Constant(2), new Negation(new Variable("x"))),
                            new Variable("y"), env);
        Test.testEvaluating(new Variable("x"), new Variable("x"), env);
        Test.testEvaluating(new Constant(2), new Assignment(new Constant(2), new Variable("x")), env);
        Test.testEvaluating(new Constant(2), new Variable("x"), env);
        Test.testEvaluating(new Constant(4), new Variable("y"), env);

        System.out.println("###############################");
    }

    public static void main(String[] args) {
        Test.testSuiteToString();
        Test.testSuiteEquals();
        Test.testSuiteEval();
        System.out.println(Test.results());
    }
}