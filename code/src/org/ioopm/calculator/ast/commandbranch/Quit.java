package org.ioopm.calculator.ast.commandbranch;
/**
 * Built-in command to exit the calculator
 */
public class Quit extends Command {
    private static final Quit theInstance = new Quit();

    private Quit() {
        type = byteEnum.Quit;
    }
    
    public static Quit instance() {
        return theInstance;
    }

    @Override
    public String toString() {
        return "Quit";
    }
}
