package org.ioopm.calculator.ast.commandbranch;
/**
 * Built-in command to clear environment's variables.
 */
public class Clear extends Command {
    private static final Clear theInstance = new Clear();
    
    private Clear() {
        type = byteEnum.Clear;
    }

    public static Clear instance() {
        return theInstance;
    }

    @Override
    public String toString() {
        return "Clear";
    }
}