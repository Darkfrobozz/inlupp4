package org.ioopm.calculator.ast.commandbranch;
/**
 * Built-in command to list all existing variables and their values.
 */
public class Vars extends Command {
    private static final Vars theInstance = new Vars();

    private Vars() {
        type = byteEnum.Vars;
    }

    public static Vars instance() {
        return theInstance;
    }

    @Override
    public String toString() {
        return "Vars";
    }
}
