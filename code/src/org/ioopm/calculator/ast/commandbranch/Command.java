package org.ioopm.calculator.ast.commandbranch;

import org.ioopm.calculator.ast.SymbolicExpression;
import org.ioopm.calculator.ast.storage.Environment;

public abstract class Command extends SymbolicExpression {
    protected byte type;
    
    @Override
    public SymbolicExpression eval(Environment env) {
        throw new IllegalStateException("Commands cannot be evaluated!");
    }

    @Override
    public boolean isCommand() {
        return true;
    }

    public byte getCommandType() {
        return type;
    }
}