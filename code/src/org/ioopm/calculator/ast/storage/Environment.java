package org.ioopm.calculator.ast.storage;

import java.util.HashMap;
import java.util.TreeSet;

import org.ioopm.calculator.ast.atombranch.Variable;
import org.ioopm.calculator.ast.SymbolicExpression;

/**
 * Representation of variable scope in AST.
 * E.g. keeps track of what value the variable "x" has.
 */
public class Environment extends HashMap<Variable, SymbolicExpression> {
    /**
     * Assigns an expression to a variable.
     * @param expr expression to assign to variable.
     * @param var variable to assign expression to.
     */
    public void assignToVariable(SymbolicExpression expr, Variable var) {
        this.put(var, expr);
    }

    /**
     * Retrieves the expression assigned to given variable.
     * @param var variable to retrieve expression from.
     * @return the expression assigned to given variable, null if no expression is assigned.
     */
    public SymbolicExpression getExprFromVariable(Variable var) {
        return this.get(var);
    }

    /**
     * Checks if the environment contains a given variable.
     * @param var variable to check if in environment.
     * @return true if environment contains the given variable, else false.
     */
    public boolean containsVariable(Variable var) {
        return this.containsKey(var);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Variables: ");
        TreeSet<Variable> vars = new TreeSet<>(this.keySet());
        
        for (Variable v : vars) {
            sb.append(v.toString());
            sb.append(" = ");
            sb.append(this.get(v));
            sb.append(", ");
        }
        // FIXME: Trailing , should not be printed.

        return sb.toString();
    }
}