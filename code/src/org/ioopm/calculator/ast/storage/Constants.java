package org.ioopm.calculator.ast.storage;

import java.util.HashMap;

import org.ioopm.calculator.ast.atombranch.NamedConstant;

import java.lang.Math;

/**
 * Built-in collection of named constants
 */
public class Constants {
    public static final HashMap<String, NamedConstant> namedConstants = new HashMap<>();

    static {
        Constants.namedConstants.put("pi", new NamedConstant("pi", Math.PI));
        Constants.namedConstants.put("e", new NamedConstant("e",  Math.E));
        Constants.namedConstants.put("Answer", new NamedConstant("Answer", 42.0));
        Constants.namedConstants.put("L", new NamedConstant("L",  6.022140857E23));
    }
}