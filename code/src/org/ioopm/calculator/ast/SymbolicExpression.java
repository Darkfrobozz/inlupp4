package org.ioopm.calculator.ast;

import java.lang.reflect.Method;

import org.ioopm.calculator.ast.storage.Environment;
import org.ioopm.calculator.ast.visitors.Visitor;
import org.ioopm.calculator.parser.IllegalExpressionException;


/**
 * Root class of the abstract syntax tree (AST).
 */
public abstract class SymbolicExpression {

    /**
     * Evaluates a symbolic expression.
     * @param env environment to evaluate expression in regards to.
     * @return a symbolic expression reduced as much as possible.
     * @throws IllegalExpressionException if evaluating assignment to a non-variable.
     */
    public abstract SymbolicExpression eval(Environment env) throws IllegalExpressionException;

    public SymbolicExpression accept(Visitor v) {
        try {
            // return v.visit(this);
            Method visitMethod = v.getClass().getMethod("visit", this.getClass());
            Object result = visitMethod.invoke(v, this);
            if (result instanceof SymbolicExpression) {
                return (SymbolicExpression) result;
            } else {
                throw new RuntimeException("Invalid subclass of SymbolicExpression.");
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getClass() + " " + e.getCause());
        }
    }

    /**
     * Checks if the object is a constant
     * @return true if object is a constant, else false
     */
    public boolean isConstant() {
        return false;
    }

    /**
     * Checks if the object is a command
     * @return true if object is a command, else false
     */
    public boolean isCommand() {
        return false;
    }

    /**
     * Checks if the object is a variable
     * @return true if object is a variable, else false
     */
    public boolean isVariable() {
        return false;
    }

    /**
     * Checks if the object is a division
     * @return true if object is a division, else false
     */
    public boolean isDivision() {
        return false;
    }
    
    /**
     * Checks if the object is a subtraction
     * @return true if object is a subtraction, else false
     */
    public boolean isSubtraction() {
        return false;
    }

    /**
     * Retrieves the name of the expressions operator.
     * @return the name of the expression's operator
     * @throws RuntimeException if called on an object with no operator.
     */
    public String getName() {
        throw new RuntimeException("getName() called on expression with no operator");
    }

    /**
     * Retrieves the value of a symbolic expression
     * @return the value
     * @throws RuntimeException if called on an object without a value
     */
    public double getValue() {
        throw new RuntimeException("getValue() called on expression without value");
    }

        
    /**
     * Retrieves the priority, in regards to evaluation order
     * @return the priority in regards to evaluation order
     */
    public int getPriority() {
        return 100;
    }

    //Assuming only one classloader
    @Override
    public boolean equals(Object other) {
        return other != null && other.getClass() == this.getClass();
    }

    public boolean containsA(Object other) {
        return equals(other);
    }
}
