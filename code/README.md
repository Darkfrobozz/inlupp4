# Assignment 3

First Java assignment in 2023 IOOPM course at Uppsala University.


## Usage
- `make all`: build .class files of all .java files in /src/
- `make run`: run Calculator
- `make test`: run tests

## Design decisions

- Exception `IllegalExpressionException` thrown when evaluating assignment to a constant is a checked expression. Since Parser 
can return expression that can throw this exception it has to be checked to ensure it is handled in Calculator (does not trash environment). 

- Division by zero throws `AritmeticException`, handled in Calculator (does not trash environment)

- getPriority() method in `SymbolicExpression` that returns a constant default priority value 100. Method is overridden in subclasses which require a lower priority value.

- HashCode() results matching equals() has not been implemented for SymbolicExpression subtypes other than for the `Variable` class. Current project only uses hashCode() of the `Variable` class.

## Test coverage

No coverage data available since tests not done via JUnit.
Automatic tests are located in the ast-package's `Test` class

## Authors
Richard Widén & Oliver Lagerqvist
2023-11-16